# Computer vision exercises

> Todos os exercícios podem ser feitos na linguagem desejada e usando bibliotecas como OpenCV, Tensorflow, Keras, etc

A linguagem utilizada foi Python na versão 3.6 juntamente com a plataforma de *data science* Anaconda. Todas as dependências são listadas no Jupyter Notebook.

Todas as questões estão resolvidas no Jupyter Notebook IA exercises.ipynb, deste repositório, com os exemplos de entrada dados e com outros baixados/gerados.

##  1) Optical flow e tracking

Selecionar uma área de um video e realizar o tracking utilizando Optical Flow. Desenhe o vetor resultante entre as localizações das features.

Utilizar imagens da câmera do dispositivo (notebook ou celular) local, caso não tenha acesso, baixar um vídeo de exemplo e anexar no resultado.

**Resultado no diretório Videos/Results/ e também ao executar o Jupyter Notebook**

Optical flow sparse: exemplo em vídeo sparseOpticalExample.mp4 e exemplo em webcam sparseOpticalWebcam.mp4

Optical flow dense: exemplo em vídeo denseOpticalExample.mp4 e exemplo em webcam denseOpticalWebcam.mp4

## 2) Image stitching

Utilizar descritores de imagem, como SURF, SIFT ou ORB para identificar descritores similares entre imagens e conecta-las, gerando uma unica imagem.

**Resultado no Jupyter notebook**

## 3) Object detection com deep learning

Detectar objetos em imagens ou vídeo utilizando um método de deep learning pre treinado com imagens do dataset ImageNet, MS COCO ou outros.

**Descrição da resolução**: A rede utilizda nesta questão foi a **YOLOV3** pré treinada com o dataset MS COCO, a rede foi escolhida por estar bastante difundida e aplicada em atividades de detecção de objetos, por sua robustez, rapidez e precisão, obtendo uma *mean average precision (mAP)* de 57.9% em vídeos de 30 frames por segundo.

**Resultado em vídeo no diretório Videos/Results/objectDetectionExample.mp4 e também em imagens ao executar o Jupyter Notebook**